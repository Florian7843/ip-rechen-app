package at.chif17.ipcalcapp.utils;

import at.chif17.ipcalcapp.iputils.IPCalculation;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class DAL {

  public static final Gson GSON = new GsonBuilder().serializeNulls().create();

  public static IPCalculation[] loadCalculations(File file) throws IOException {
    if (!file.exists()) file.createNewFile();
    FileReader fr = new FileReader(file);
    return GSON.fromJson(fr, IPCalculation[].class);
  }

  public static void saveCalculations(File file, IPCalculation[] calculations) throws IOException {
    if (!file.exists()) file.createNewFile();
    FileWriter fw = new FileWriter(file, false);
    fw.write(GSON.toJson(calculations));
    fw.flush();

  }

}
