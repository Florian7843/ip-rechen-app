package at.chif17.ipcalcapp.iputils;

public class IPCalculation {

  private IPv4Address address;
  private Prefix toPrefix;

  public IPCalculation(IPv4Address address, Prefix toPrefix) {
    this.address = address;
    this.toPrefix = toPrefix;
  }

  public IPv4Address getAddress() {
    return address;
  }

  public Prefix getToPrefix() {
    return toPrefix;
  }
}
