package at.chif17.ipcalcapp.iputils;

public class IPCalculator {

  private static String matchBinaryIPv4AddressWithPrefix(IPv4Address iPv4Address, Prefix toPrefix, boolean hostBitsToOne, boolean subnetBitsToOne, boolean useCustomSubnetNumber, int customSubnetNumber) {
    StringBuilder sb = new StringBuilder();

    int subnetPartMin = iPv4Address.getPrefix().getAsInt();
    int subnetPartMax = toPrefix.getAsInt();

    String ipv4Binary = iPv4Address.toBinaryString().replaceAll(" ", "");

    StringBuilder cSubSB = new StringBuilder(Integer.toBinaryString(customSubnetNumber));
    while (subnetPartMax - subnetPartMin > cSubSB.length()) cSubSB.insert(0, "0");
    String customSubnetString = cSubSB.toString();

    for (int i = 0; i < 32; i++) {

      boolean isNetworkPart = i < iPv4Address.getPrefix().getAsInt();
      boolean isSubnetPart = i >= iPv4Address.getPrefix().getAsInt() && i < toPrefix.getAsInt();
      boolean isHostPart = i >= toPrefix.getAsInt();

      if (isNetworkPart) {
        sb.append(ipv4Binary.charAt(i));
      } else if (isSubnetPart) {
        if (useCustomSubnetNumber) {
          sb.append(customSubnetString.charAt(i - subnetPartMin));
        } else {
          sb.append((subnetBitsToOne) ? 1 : 0);
        }
      } else if (isHostPart) {
        sb.append((hostBitsToOne) ? 1 : 0);
      }

      if ((i + 1) % 8 == 0) sb.append(" ");
    }

    return sb.toString();
  }

  private static String matchBinaryIPv4AddressWithPrefix(IPv4Address iPv4Address, Prefix toPrefix, boolean hostBitsToOne, int customSubnetNumber) {
    return matchBinaryIPv4AddressWithPrefix(iPv4Address, toPrefix, hostBitsToOne, false, true, customSubnetNumber);
  }

  private static String matchBinaryIPv4AddressWithPrefix(IPv4Address iPv4Address, Prefix toPrefix, boolean hostBitsToOne, boolean subnetBitsToOne) {
    return matchBinaryIPv4AddressWithPrefix(iPv4Address, toPrefix, hostBitsToOne, subnetBitsToOne, false, 0);
  }

  private static String convertBinaryToIPv4Address(String binaryString) {
    StringBuilder sb = new StringBuilder();
    String[] binarySplit = binaryString.split(" ");

    for (int i = 0; i < binarySplit.length; i++) {
      sb.append(Integer.parseInt(binarySplit[i], 2));
      if (i < binarySplit.length - 1) sb.append(".");
    }

    return sb.toString();
  }

  //<editor-fold desc="network specific calculation">
  public static String getBinaryNetworkAddress(IPv4Address iPv4Address, Prefix toPrefix) {
    return matchBinaryIPv4AddressWithPrefix(iPv4Address, toPrefix, false, false);
  }

  public static String getNetworkAddress(IPv4Address iPv4Address, Prefix toPrefix) {
    return convertBinaryToIPv4Address(getBinaryNetworkAddress(iPv4Address, toPrefix));
  }

  public static String getBinaryNetworkBroadcast(IPv4Address iPv4Address) {
    return matchBinaryIPv4AddressWithPrefix(iPv4Address, iPv4Address.getPrefix(), true, true);
  }

  public static String getNetworkBroadcast(IPv4Address iPv4Address) {
    return convertBinaryToIPv4Address(getBinaryNetworkBroadcast(iPv4Address));
  }
  //</editor-fold>

  //<editor-fold desc="subnet specific calculation">
  public static String getBinarySubnetAddress(IPv4Address iPv4Address, Prefix toPrefix, int subnetNumber) {
    return matchBinaryIPv4AddressWithPrefix(iPv4Address, toPrefix, false, subnetNumber);
  }

  public static String getSubnetAddress(IPv4Address iPv4Address, Prefix toPrefix, int subnetNumber) {
    return convertBinaryToIPv4Address(getBinarySubnetAddress(iPv4Address, toPrefix, subnetNumber));
  }

  public static String getBinarySubnetBroadcast(IPv4Address iPv4Address, Prefix toPrefix, int subnetNumber) {
    return matchBinaryIPv4AddressWithPrefix(iPv4Address, toPrefix, true, subnetNumber);
  }

  public static String getSubnetBroadcast(IPv4Address iPv4Address, Prefix toPrefix, int subnetNumber) {
    return convertBinaryToIPv4Address(getBinarySubnetBroadcast(iPv4Address, toPrefix, subnetNumber));
  }

  public static String getBinarySubnetFirstRouterAddress(IPv4Address iPv4Address, Prefix toPrefix, int subnetNumber) {
    String binarySubnetAddress = getBinarySubnetAddress(iPv4Address, toPrefix, subnetNumber);
    return binarySubnetAddress.substring(0, binarySubnetAddress.length() - 2).concat("1");
  }

  public static String getSubnetFirstRouterAddress(IPv4Address iPv4Address, Prefix toPrefix, int subnetNumber) {
    return convertBinaryToIPv4Address(getBinarySubnetFirstRouterAddress(iPv4Address, toPrefix, subnetNumber));
  }

  public static String getBinarySubnetLastRouterAddress(IPv4Address iPv4Address, Prefix toPrefix, int subnetNumber) {
    String binarySubnetAddress = getBinarySubnetBroadcast(iPv4Address, toPrefix, subnetNumber);
    return binarySubnetAddress.substring(0, binarySubnetAddress.length() - 2).concat("0");
  }

  public static String getSubnetLastRouterAddress(IPv4Address iPv4Address, Prefix toPrefix, int subnetNumber) {
    return convertBinaryToIPv4Address(getBinarySubnetLastRouterAddress(iPv4Address, toPrefix, subnetNumber));
  }

  //</editor-fold>


  public static int calculateAmountOfSubnets(IPv4Address iPv4Address, Prefix toPrefix) {
    int from = iPv4Address.getPrefix().getAsInt();
    int to = toPrefix.getAsInt();

    return (int) Math.pow(2, to - from);
  }

}
