package at.chif17.ipcalcapp.iputils;

public class Prefix {

  private int prefix;

  public Prefix(int prefix) throws IllegalArgumentException {
    if (prefix < 0 || prefix > 32) throw new IllegalArgumentException("Prefix must be 0 - 32");
    this.prefix = prefix;
  }

  public int getAsInt() {
    return prefix;
  }

  public String toSubnetMask() {
    StringBuilder sb = new StringBuilder();

    int addedParts = 0;
    int pre8 = prefix / 8;
    for (int i = 0; i < pre8; i++) {
      sb.append("255");
      if (addedParts < 4) sb.append(".");
      addedParts++;
    }

    int calc = (int) (Math.pow(2, 8) - Math.pow(2, 8 - prefix % 8));
    sb.append(calc);
    addedParts++;

    for (int i = addedParts; i < 4; i++) {
      sb.append(".0");
    }

    return sb.toString();
  }

  public String toBinarySubnetMask() {
    StringBuilder sb = new StringBuilder();

    for (int i = 0; i < 32; i++) {
      sb.append((prefix > i ? 1 : 0));
      if (i != 0 && (i + 1) % 8 == 0) sb.append(" ");
    }

    return sb.toString();
  }
}
