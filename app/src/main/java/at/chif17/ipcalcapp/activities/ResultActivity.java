package at.chif17.ipcalcapp.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import at.chif17.ipcalcapp.R;
import at.chif17.ipcalcapp.iputils.IPCalculator;
import at.chif17.ipcalcapp.iputils.IPv4Address;
import at.chif17.ipcalcapp.iputils.Prefix;

public class ResultActivity extends AppCompatActivity {

  private TextView tvIPAddressValue;
  private TextView tvSubnetMaskValue;
  private TextView tvNetworkAddressValue;
  private TextView tvNetworkBroadcastValue;

  private TextView tvAmountHostsValue;
  private TextView tvBitsNetworkValue;
  private TextView tvBitsSubnetsValue;
  private TextView tvBitsHostsValue;

  private TextView tvSubnetAddressValue;
  private TextView tvSubnetBroadcastValue;
  private TextView tvSubnetFirstRouterValue;
  private TextView tvSubnetLastRouterValue;

  private TextView tvSubnetNumberCurrent;
  private TextView tvSubnetNumberLowest;
  private TextView tvSubnetNumberHighest;

  private SeekBar sbSubnetBar;

  private IPv4Address address;
  private Prefix toPrefix;
  private boolean binary = false;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_result);

    tvIPAddressValue = findViewById(R.id.tvIPAddressValue);
    tvSubnetMaskValue = findViewById(R.id.tvSubnetMaskValue);
    tvNetworkAddressValue = findViewById(R.id.tvNetworkAddressValue);
    tvNetworkBroadcastValue = findViewById(R.id.tvNetworkBroadcastValue);

    tvAmountHostsValue = findViewById(R.id.tvAmountHostsValue);
    tvBitsNetworkValue = findViewById(R.id.tvBitsNetworkValue);
    tvBitsSubnetsValue = findViewById(R.id.tvBitsSubnetsValue);
    tvBitsHostsValue = findViewById(R.id.tvBitsHostsValue);

    tvSubnetAddressValue = findViewById(R.id.tvSubnetAddressValue);
    tvSubnetBroadcastValue = findViewById(R.id.tvSubnetBroadcastValue);
    tvSubnetFirstRouterValue = findViewById(R.id.tvSubnetFirstRouterValue);
    tvSubnetLastRouterValue = findViewById(R.id.tvSubnetLastRouterValue);

    tvSubnetNumberCurrent = findViewById(R.id.tvSubnetNumberCurrent);
    tvSubnetNumberLowest = findViewById(R.id.tvSubnetNumberLowest);
    tvSubnetNumberHighest = findViewById(R.id.tvSubnetNumberHighest);

    sbSubnetBar = findViewById(R.id.sbSubnetBar);

    sbSubnetBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
      @Override
      public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        Resources res = getResources();
        String text = res.getString(R.string.subnet_nr, i);

        tvSubnetNumberCurrent.setText(text);

        calculateSubnetSpecificAddresses(address, toPrefix, binary, sbSubnetBar.getProgress());
      }

      @Override
      public void onStartTrackingTouch(SeekBar seekBar) {

      }

      @Override
      public void onStopTrackingTouch(SeekBar seekBar) {

      }
    });

    Intent intent = getIntent();

    if (intent != null && intent.getStringExtra("ip-address") != null) {
      address = IPv4Address.fromString(intent.getStringExtra("ip-address"));
      toPrefix = new Prefix(intent.getIntExtra("prefixTo", 24));
      binary = intent.getBooleanExtra("binaryAddresses", false);

      calculateNetworkAddresses(address, toPrefix, binary);
    } else finish();
  }

  private void calculateNetworkAddresses(IPv4Address address, Prefix toPrefix, boolean binary) {
    Resources res = getResources();
    int subnetAmount = IPCalculator.calculateAmountOfSubnets(address, toPrefix);

    tvAmountHostsValue.setText(res.getString(R.string.x_hosts, (int) Math.pow(2, 32 - toPrefix.getAsInt())));
    tvBitsNetworkValue.setText(res.getString(R.string.x_bits, address.getPrefix().getAsInt()));
    tvBitsSubnetsValue.setText(res.getString(R.string.x_bits, toPrefix.getAsInt() - address.getPrefix().getAsInt()));
    tvBitsHostsValue.setText(res.getString(R.string.x_bits, 32 - toPrefix.getAsInt()));

    tvSubnetNumberHighest.setText(String.valueOf(subnetAmount));
    tvSubnetNumberCurrent.setText(String.valueOf(0));
    sbSubnetBar.setMax(subnetAmount - 1);

    if (!binary) {
      tvIPAddressValue.setText(address.toString() + " auf /" + toPrefix.getAsInt());
      tvSubnetMaskValue.setText(toPrefix.toSubnetMask());
      tvNetworkAddressValue.setText(IPCalculator.getNetworkAddress(address, toPrefix));
      tvNetworkBroadcastValue.setText(IPCalculator.getNetworkBroadcast(address));
    } else {
      tvIPAddressValue.setText(address.toBinaryString());
      tvSubnetMaskValue.setText(toPrefix.toBinarySubnetMask());
      tvNetworkAddressValue.setText(IPCalculator.getBinaryNetworkAddress(address, toPrefix));
      tvNetworkBroadcastValue.setText(IPCalculator.getBinaryNetworkBroadcast(address));
    }
  }

  private void calculateSubnetSpecificAddresses(IPv4Address address, Prefix toPrefix, boolean binary, int subnetNumber) {
    if (!binary) {
      tvSubnetAddressValue.setText(IPCalculator.getSubnetAddress(address, toPrefix, subnetNumber));
      tvSubnetBroadcastValue.setText(IPCalculator.getSubnetBroadcast(address, toPrefix, subnetNumber));
      tvSubnetFirstRouterValue.setText(IPCalculator.getSubnetFirstRouterAddress(address, toPrefix, subnetNumber));
      tvSubnetLastRouterValue.setText(IPCalculator.getSubnetLastRouterAddress(address, toPrefix, subnetNumber));
    } else {
      tvSubnetAddressValue.setText(IPCalculator.getBinarySubnetAddress(address, toPrefix, subnetNumber));
      tvSubnetBroadcastValue.setText(IPCalculator.getBinarySubnetBroadcast(address, toPrefix, subnetNumber));
      tvSubnetFirstRouterValue.setText(IPCalculator.getBinarySubnetFirstRouterAddress(address, toPrefix, subnetNumber));
      tvSubnetLastRouterValue.setText(IPCalculator.getBinarySubnetLastRouterAddress(address, toPrefix, subnetNumber));
    }
  }
}
