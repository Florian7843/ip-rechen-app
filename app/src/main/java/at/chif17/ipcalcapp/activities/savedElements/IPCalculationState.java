package at.chif17.ipcalcapp.activities.savedElements;

import android.view.View;
import at.chif17.ipcalcapp.iputils.IPCalculation;

public class IPCalculationState {

  private final IPCalculation ipCalculation;
  private View view;
  private boolean state;

  public IPCalculationState(IPCalculation ipCalculation) {
    this.ipCalculation = ipCalculation;
    state = false;
  }

  public IPCalculation getIpCalculation() {
    return ipCalculation;
  }

  public boolean state() {
    return state;
  }

  public void setState(boolean state) {
    this.state = state;
  }

  public View getView() {
    return view;
  }

  public void setView(View view) {
    this.view = view;
  }
}
