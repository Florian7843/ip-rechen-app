package at.chif17.ipcalcapp.activities.savedElements;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import at.chif17.ipcalcapp.R;
import at.chif17.ipcalcapp.activities.SavedElementsActivity;
import at.chif17.ipcalcapp.iputils.IPCalculation;
import at.chif17.ipcalcapp.iputils.IPv4Address;
import at.chif17.ipcalcapp.utils.DAL;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SavedElementsAdapter extends RecyclerView.Adapter<SavedElementsViewHolder> {

  private Context context;
  private List<IPCalculationState> dataset = new ArrayList<>();
  private SavedElementsActivity savedElementsActivity;

  public SavedElementsAdapter(Context context, SavedElementsActivity savedElementsActivity) {
    this.context = context;
    this.savedElementsActivity = savedElementsActivity;
    try {
      IPCalculation[] calculations = DAL.loadCalculations(new File(context.getFilesDir(), "addresses.txt"));
      for (IPCalculation calculation : calculations) {
        dataset.add(new IPCalculationState(calculation));
      }
    } catch (IOException e) {
      dataset = new ArrayList<>();
      Toast.makeText(context, "Couldn't load saved IP-Calculations", Toast.LENGTH_LONG).show();
      e.printStackTrace();
    }
  }

  @NonNull
  @Override
  public SavedElementsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.saved_elements_row, parent, false);
    return new SavedElementsViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull SavedElementsViewHolder holder, int i) {
    final IPCalculationState calculationState = dataset.get(i);
    calculationState.setView(holder.row);
    final IPCalculation ipCalculation = calculationState.getIpCalculation();
    IPv4Address address = ipCalculation.getAddress();

    holder.tvIPv4Address.setText(String.format(
        context.getResources().getString(R.string.ipv4_address_placeholder),
        address.getIpp1(),
        address.getIpp2(),
        address.getIpp3(),
        address.getIpp4(),
        address.getPrefix().getAsInt()
    ));

    holder.tvPrefixTo.setText(String.format(context.getResources().getString(R.string.prefix_placeholder), ipCalculation.getToPrefix().getAsInt()));

    holder.row.setOnClickListener(view -> {
      boolean isSelecting = false;

      for (IPCalculationState state : dataset) {
        if (state.state()) {
          isSelecting = true;
          break;
        }
      }

      if (!isSelecting) savedElementsActivity.loadCalculation(ipCalculation);
      else updateSelection(calculationState);
    });
    holder.row.setOnLongClickListener(view -> updateSelection(calculationState));
  }

  private boolean updateSelection(IPCalculationState calculationState) {
    View view = calculationState.getView();
    calculationState.setState(!calculationState.state());
    view.setBackgroundColor(calculationState.state() ? Color.LTGRAY : Color.WHITE);

    return true;
  }

  public void unselectAll() {
    for (IPCalculationState state : dataset) {
      if (state.state()) updateSelection(state);
      state.getView().setBackgroundColor(Color.WHITE);
    }
  }

  public void deleteAllSelectedElements() {
    List<IPCalculationState> toRemove = new ArrayList<>();
    List<IPCalculation> calculations = new ArrayList<>();

    for (IPCalculationState state : dataset) {
      if (state.state()) toRemove.add(state);
      else calculations.add(state.getIpCalculation());
    }

    dataset.removeAll(toRemove);
    this.notifyDataSetChanged();

    File addressesFile = new File(context.getFilesDir(), "addresses.txt");
    try {
      DAL.saveCalculations(addressesFile, calculations.toArray(new IPCalculation[0]));
    } catch (IOException e) {
      Toast.makeText(context, "Couldn't save deleting elements", Toast.LENGTH_LONG);
    }

    unselectAll();
  }

  @Override
  public int getItemCount() {
    return dataset.size();
  }


}
