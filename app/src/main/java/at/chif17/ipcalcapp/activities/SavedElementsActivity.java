package at.chif17.ipcalcapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import at.chif17.ipcalcapp.R;
import at.chif17.ipcalcapp.activities.savedElements.SavedElementsAdapter;
import at.chif17.ipcalcapp.iputils.IPCalculation;

public class SavedElementsActivity extends AppCompatActivity {

    private RecyclerView rvSavedElements;
    private SavedElementsAdapter rvAdapter;
    private RecyclerView.LayoutManager rvLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_elements);

        rvSavedElements = findViewById(R.id.rvSavedElements);
        rvLayoutManager = new LinearLayoutManager(this);
        rvSavedElements.setLayoutManager(rvLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvSavedElements.getContext(), DividerItemDecoration.VERTICAL);
        rvSavedElements.addItemDecoration(dividerItemDecoration);

        rvAdapter = new SavedElementsAdapter(getApplicationContext(), this);
        rvSavedElements.setAdapter(rvAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_saved_elements, menu);
        return true;
    }

    public void loadCalculation(IPCalculation calculation) {
        Intent intent = new Intent();
        intent.putExtra("ipv4address", calculation.getAddress().toString());
        intent.putExtra("toPrefix", calculation.getToPrefix().getAsInt());

        setResult(RESULT_OK, intent);
        finish();
    }

    public void deleteSelected(MenuItem item) {
        rvAdapter.deleteAllSelectedElements();
    }

    public void unselectAll(MenuItem item) {
        rvAdapter.unselectAll();
    }
}
