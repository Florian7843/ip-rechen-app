package at.chif17.ipcalcapp.activities.savedElements;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import at.chif17.ipcalcapp.R;

public class SavedElementsViewHolder extends RecyclerView.ViewHolder {

  public TextView tvIPv4Address;
  public TextView tvPrefixTo;

  public View row;

  public SavedElementsViewHolder(@NonNull View v) {
    super(v);

    tvIPv4Address = v.findViewById(R.id.tvIPv4Address);
    tvPrefixTo = v.findViewById(R.id.tvPrefixTo);

    row = v;

  }


}
