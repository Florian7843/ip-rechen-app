package at.chif17.ipcalcapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import at.chif17.ipcalcapp.R;
import at.chif17.ipcalcapp.iputils.IPCalculation;
import at.chif17.ipcalcapp.iputils.IPv4Address;
import at.chif17.ipcalcapp.iputils.Prefix;
import at.chif17.ipcalcapp.utils.DAL;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private EditText etIp1;
    private EditText etIp2;
    private EditText etIp3;
    private EditText etIp4;
    private EditText etIpPrefix;
    private EditText etPrefixTo;

    private CheckBox cbBinaryAddresses;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etIp1 = findViewById(R.id.etIp1);
        etIp2 = findViewById(R.id.etIp2);
        etIp3 = findViewById(R.id.etIp3);
        etIp4 = findViewById(R.id.etIp4);
        etIpPrefix = findViewById(R.id.etIpPrefix);
        etPrefixTo = findViewById(R.id.etPrefixTo);

        cbBinaryAddresses = findViewById(R.id.cbBinaryAddress);
    }

    public void calculate(View view) {
        IPv4Address address = getEnteredIPv4Address();

        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra("ip-address", address.toString());
        intent.putExtra("prefixTo", Integer.parseInt(String.valueOf(etPrefixTo.getText())));
        intent.putExtra("binaryAddresses", cbBinaryAddresses.isChecked());

        startActivity(intent);
    }

    private IPv4Address getEnteredIPv4Address() {
        return new IPv4Address(
            Integer.parseInt(String.valueOf(etIp1.getText())),
            Integer.parseInt(String.valueOf(etIp2.getText())),
            Integer.parseInt(String.valueOf(etIp3.getText())),
            Integer.parseInt(String.valueOf(etIp4.getText())),
            new Prefix(Integer.parseInt(String.valueOf(etIpPrefix.getText())))
        );
    }

    public void save(View view) throws Exception {
        File addressesFile = new File(getFilesDir(), "addresses.txt");
        IPCalculation[] calculations = DAL.loadCalculations(addressesFile);

        ArrayList<IPCalculation> input = new ArrayList<>(Arrays.asList(calculations));

        input.add(new IPCalculation(
            getEnteredIPv4Address(),
            new Prefix(Integer.parseInt(String.valueOf(etPrefixTo.getText())))
        ));

        DAL.saveCalculations(addressesFile, input.toArray(new IPCalculation[0]));
        Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();

    }

    public void showSaved(View view) {
        Intent intent = new Intent(this, SavedElementsActivity.class);
        startActivityForResult(intent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 0) {
            if (resultCode != RESULT_OK || data == null) return;
            if (!(data.hasExtra("ipv4address") && data.hasExtra("toPrefix"))) return;

            IPv4Address iPv4Address = IPv4Address.fromString(data.getStringExtra("ipv4address"));
            Prefix toPrefix = new Prefix(data.getIntExtra("toPrefix", 24));

            Log.i("asd", iPv4Address.toString() + " " + toPrefix.getAsInt());

            etIp1.setText(String.valueOf(iPv4Address.getIpp1()));
            etIp2.setText(String.valueOf(iPv4Address.getIpp2()));
            etIp3.setText(String.valueOf(iPv4Address.getIpp3()));
            etIp4.setText(String.valueOf(iPv4Address.getIpp4()));
            etIpPrefix.setText(String.valueOf(iPv4Address.getPrefix().getAsInt()));
            etPrefixTo.setText(String.valueOf(toPrefix.getAsInt()));
        }
    }
}
